# [SECTION] Comments
# Comments in python are done using the "#" symbol

# [SECTION] python Syntax

print("Hello World!")

# [SECTION] Indentation 
# Indentation in python is very important
# In python, indentation is used to indicate the block of code

# [SECTION] Variables
# The terminology used for variable name is "identifier"
# In python , a variable is declared by stating the variable name and assigning a value in equality symbol

# [SECTION] Naming Conventions
# Python uses the snake case convention

age = 35;
print(age)

middle_initial = "C"
print(middle_initial)

# Python allows assigning of values to multiple variables in one line

name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"
print(name1)
print(name4)

# [SECTION] Data Types

# 1. Strings - for alphanumeric and symbols
full_name = "John Doe"
print (full_name)

secret_code = 'Pa$$word'
print (secret_code)

# 2. Numbers(int, float, complex) - for integers, decimals and complex numbers
num_of_days = 365 # This is an integer
print(num_of_days)
pi_approx = 3.1416 # This is s decimal
print(pi_approx)
complex_num = 1 + 5j # This is a complex number
print(complex_num)
print(complex_num.real)
print(complex_num.imag)

# Booleans - for truth values

is_learning = True
is_difficult = False
print(is_learning)
print(is_difficult)

# [SECTION] Using Variables
# Just like in JS, variables are used by simply calling the name of the identifiers
# To use variables, concatanation ("+") symbol between strings can be used
print("My name is " + full_name)

# This returns a "Type" error as numbers can't be concatinated to strings
# print("My age is " + age)

print("My age is " + str(age))

# [SECTION] Typecasting
# There maybe times when we want to specify a type on to a variable. This can be done with ca sting, Here are some functions that can be used.
# 1. int() - converts the value to a integer or a whole number
# 2. float() - convers the value to a float value
# 3. str() - converts the value into strings
print(int(3.5))
print(float(3))

# Another way to avoid the type error in printing without the use of typecasting is the use of F-strings
# To use the F-strings, add the lowercase "f" before the string and place the desired variable in {} 
print(f"Hi, my name is {full_name} and my age is {age}")

# [SECTION] Operations
# Python  has operator families that can be used to manipulate variables
# Arithmetic operators - perform mathematical operations

print(1 + 10)
print(15 - 8)
print(18 * 9)
print(21 / 7)
print(18 % 4) # modulo displays the remainder
print(2 ** 6) # exponent

# Assignments Operators - 
num1 = 3
print(num1)
num1 += 4
print(num1)